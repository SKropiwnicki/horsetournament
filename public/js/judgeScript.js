/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false */
"use strict";


$(function(){



    var TRate = $("#TRate");
    var GRate = $("#GRate");
    var KRate = $("#KRate");
    var NRate = $("#NRate");
    var RRate = $("#RRate");
    var send = $("#SubmitRate");
    var horseID = $("#horseID");
    var confirmCode = $("#codeConfirm");
    var codeValue = $("#codeInput");
    var msg = $("#msg");

    var judgeID = -1;

    var canRate = false;

    var socket;


    socket = io({forceNew: true});



    socket.emit("askHorseID");

    socket.on("getHorseID", function(horse_ID){
        horseID.empty();
      horseID.append(horse_ID);
    });

    socket.on("msg", function(msg1){
        console.log(msg);
        msg.html(msg1.toString());
    });

    socket.on("msgCanRate", function(msg1){
        if (canRate) { msg.html(msg1.toString()); }
    });

    socket.on("checkJudges", function () {
       socket.emit('changeJudges');
    });

    socket.on("finish", function() {
        $("#AllRate").hide();
        $("#status").hide();
        msg.html("Tournament is finished. Thank you for your cooperation");
    });

    socket.on("checkCanRate", function(answer){
        msg.html(answer.msg.toString());
        if(answer.isChosen === true){
            $("#status").show();
            $("#AllRate").show();
            canRate = true;
        }
        else
        {
            console.log("Poczekaj");
            $("#AllRate").hide();
            $("#status").show();
            canRate = false;
        }
    });


    socket.on("idAnswer", function(answer){
        console.log("Otrzymałem odpowiedz na zapytanie identyfikacyjne");
        if (answer.bool === true){
            console.log("kod jest ok");
            $("#codeForm").hide();
            msg.html(answer.msg.toString());
            judgeID = answer._idJudge;
            $("#judgeName").html(answer.judgeName.toString());
            if(answer.isChosen === true){
                $("#status").show();
                $("#AllRate").show();
                canRate = true;
            }
            else
            {
                $("#AllRate").hide();
                $("#status").show();
                canRate = false;
            }
        }else
        {
            msg.html(answer.msg.toString());
        }

    });

    socket.on("resetValues", function(){
        console.log("Resetuje ")
        $("#TRate").val(0);
        $("#GRate").val(0);
        $("#KRate").val(0);
        $("#NRate").val(0);
        $("#RRate").val(0);
        $("#TValue").html(0);
        $("#GValue").html(0);
        $("#KValue").html(0);
        $("#NValue").html(0);
        $("#RValue").html(0);
    });

    $("#TValue").html(TRate.val().toString());
    $("#GValue").html(GRate.val().toString());
    $("#KValue").html(KRate.val().toString());
    $("#NValue").html(NRate.val().toString());
    $("#RValue").html(RRate.val().toString());


    TRate.bind("input change", function(){
       $("#TValue").html(TRate.val().toString())
    });
    GRate.bind("input change", function(){
        $("#GValue").html(GRate.val().toString())
    });
    KRate.bind("input change", function(){
        $("#KValue").html(KRate.val().toString())
    });
    NRate.bind("input change", function(){
        $("#NValue").html(NRate.val().toString())
    });
    RRate.bind("input change", function(){
        $("#RValue").html(RRate.val().toString())
    });




    //identyfikacja
    confirmCode.click( function (e) {
        e.preventDefault();
        console.log("Clicked confirm code");
        socket.emit('identification', codeValue.val());
        console.log('I sent msg: ' + codeValue.val());
    });


    send.click( function (e) {
        e.preventDefault();
        var rating = {
            T: TRate.val(),
            G: GRate.val(),
            K: KRate.val(),
            N: NRate.val(),
            R: RRate.val(),
            _horse: horseID.val(),
            _judge: judgeID
        };
        socket.emit('addRating', rating);
        console.log('I sent msg: ' + horseID.val() );
    });
});
