/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false */
"use strict";


$(function(){



    var horseID = $("#horseID");
    var ratings = $("#ratings");
    var maxratings = $("#maxratings");
    var showCodes = $("#showCodes");
    var nextHorse = $("#NextHorse");
    var warnJudges= $("#warnJudges");



    var socket;

    if (!socket || !socket.connected) {
        socket = io({forceNew: true});
    }

    socket.emit("askHorseID");
    socket.emit("askRatingsNumber");

    socket.on("getHorseID", function(horse_ID){
            horseID.empty();
            horseID.append(horse_ID);
        }
    );

    socket.on("getRatingsNumber", function(ratingsData, maxratingsData){
        ratings.empty();
        ratings.append(ratingsData);
        maxratings.empty();
        maxratings.append(maxratingsData);
    });


    socket.on("msg", function(msg){
        console.log(msg);
    });

    socket.on("finishAdmin", function ()
    {
        nextHorse.attr("disabled", true);
        $("#message").html("TOURNAMENT IS FINISHED");
        console.log("KONIEC ZAWODÓW");
    });

    nextHorse.click( function () {
        console.log("Kliknieto next horse");
       socket.emit('nextHorse');
    });

    warnJudges.click(function () {
        socket.emit('warnJudges');
    });
    showCodes.click( function () {

        socket.emit('showCodes');
        console.log('I sent msg to show Current Codes');
    });
});
