/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false */
"use strict";

$(function(){

    var socket ;
    console.log("Witam!");

    if (!socket || !socket.connected) {
        socket = io({forceNew: true});
    }

    socket.emit("askForUpdate");

    var table = document.getElementById("finalRanking");
    var msg = $("#msg");


    socket.on("lastRatings", function(data, horseName){
        $("#last").html("");
        $("#lastHorse").html("");
        $("#last").html(
            "<tr><th>"
            + "Judge Name" + "</th><th>"
            + "T" + "</th><th>"
            + "G" + "</th><th>"
            + "K" + "</th><th>"
            + "N" + "</th><th>"
            + "R" + "</th>"
            + "</tr>");
        $("#lastHorse").append(horseName.toString() + " detailed ratings:");
        for(var i=0; i<data.length; i++){
            $("#last").append("<tr><td>"
                + data[i]._judge.firstname+" "+ data[i]._judge.surname +"</td><td>"
                + data[i].T + "</td><td>"
                + data[i].G + "</td><td>"
                + data[i].K + "</td><td>"
                + data[i].N + "</td><td>"
                + data[i].R + "</td>"
                + "</tr>");
        }
    });



    socket.on("addRow", function(data) {
        console.log("Wrzucam nowy row w odpowiednie miejsce");
        if (data) {
            makeRow(data, table);
        }
    });

    socket.on("finish", function() {
        msg.html("Tournament is finished. Thank you for watching!");
    });

    socket.on("getActualView", function(data) {
        console.log("Pobieram wszystkie obecne do tej pory komórki");

        for (var i = 0; i < data.length; i++) {
            makeRow(data[i], table);
        }
    });

    var makeRow = function (data, table) {
        var i = 0;
        $("#finalRanking tr").each(function () {
            if (i !== 0) {
                var averAll = $(table.rows[i].cells[2]).text();
                if (data.averageAll > averAll) {
                    return;
                } else if (data.averageAll == averAll) {
                    var averT = $(table.rows[i].cells[3]).text();
                    if (data.averageT > averT) {
                        return;
                    } else if (data.averageT == averT) {
                        var averR = $(table.rows[i].cells[4]).text();
                        if (data.averageR > averR) {
                            return;
                        } else if (data.averageR == averR) {
                            //są równe
                        }
                    }
                }
            }
            i++;
        });
        console.log(i);
        var row = table.insertRow(i);
        var c0 = row.insertCell(0);
        var c1 = row.insertCell(1);
        var c2 = row.insertCell(2);
        var c3 = row.insertCell(3);
        var c4 = row.insertCell(4);
        c0.innerHTML = i.toString();
        c1.innerHTML = data.name;
        c2.innerHTML = data.averageAll.toFixed(2);
        c3.innerHTML = data.averageT.toFixed(2);
        c4.innerHTML = data.averageR.toFixed(2);


        i=0;
        $("#finalRanking tr").each(function (){
            if (i !== 0){
                table.rows[i].cells[0].innerHTML = i;
            }
            i++;
        })
    };
});


