var mongoose = require('mongoose');
var async = require('async');
var Horse = require('./Horse');
var Rating = require('./Rating');
var Judge = require('./Judge');



mongoose.connect('mongodb://localhost/HorseTournament');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var isCreated = false;
var horseNumber = 5;
var judgeNumber = 10;


/*

var ratingSchema = mongoose.Schema( {
    _horse:{type: Number, ref: 'Horse'},
    T: Number,
    G: Number,
    K: Number,
    N: Number,
    R: Number,
    _judge:{type: Number, ref: 'Judge'}
});


var horseSchema = mongoose.Schema({
    _id: Number,
    name: String,
    ratings: [{type: Schema.Types.ObjectId , ref: 'Rating'}]
});

var judgeSchema = mongoose.Schema({
    _id: Number,
    firstname: String,
    surname: String,
    country: String,
    ratings: [{type: Schema.Types.ObjectId, ref: 'Rating'}]
});


var Horse = mongoose.model('Horse', horseSchema);
var Rating = mongoose.model('Rating', ratingSchema);
var Judge = mongoose.model('Judge', judgeSchema);
*/


var dbControlSchema = mongoose.Schema({
    isCreated: Boolean
});


var dbControl = mongoose.model('dbControl', dbControlSchema);

var connectDB = function (connectDone){
    db.once('open', function() {
        console.log("Połaczone");
        connectDone();

    })
};

var initiateDB = function (initiateDone){
    isCreated=false;
    console.log("Rozpoczynam initiate");
    dbControl.findOne({'isCreated': true}, function (err, result) {
        if (err) { handleError(err); }
        if(result !== null) if (result.isCreated) {
            isCreated = true;
            console.log("Wykrylem istniejaca baze danych");
        }
        console.log("Koncze initiate");
        initiateDone();
    });


};




var createDB = function (createDone){
    if (isCreated !== true) {
        console.log("Robimy baze danych");
        async.series([
            function (horsesSaveDone) {
                var horseRandomNames = randomHorses(horseNumber);
                var j = 0;
/*

                function asyncLoop(i, callback) {
                    if (i < horseNumber){
                        var currentHorse= new Horse({});
                        currentHorse.name = horseRandomNames[i];
                        currentHorse._id = i;
                        console.log(i+" "+currentHorse);
                        currentHorse.save(function (err) {
                            if (err) return handleError(err);
                            console.log("saveuje "+i);
                            asyncLoop(i+1, callback);
                        })
                    }
                    else {
                        console.log("i> horseNumber");
                        callback();
                    }
                } asyncLoop(0, function(){
                    console.log("Koniec async loopa");
                   horsesSaveDone();
                });
*/
                for (var i = 0; i< horseNumber; i++ ){
                    var currentHorse= new Horse({});
                    currentHorse.name = horseRandomNames[i];
                    currentHorse._id = i;
                    console.log(j+" "+currentHorse);
                    j++;
                    (function (number) {
                    currentHorse.save(function (err) {
                        if (err) return handleError(err);
                        console.log(number);
                        if (number === horseNumber - 1)horsesSaveDone();
                    });
                    }(j));
                }
            },
           function (judgesSaveDone){
                var randomJ = randomJudges(judgeNumber);
                var j=0;
                for (var i = 0; i< judgeNumber; i++ ){
                    var currentJudge= new Judge({});
                    currentJudge.firstname = randomJ[i].firstname;
                    currentJudge.surname = randomJ[i].surname;
                    currentJudge.country = randomJ[i].country;
                    currentJudge.code = randomJ[i].code;
                    currentJudge._id = i;
                    console.log(j+" "+currentJudge);
                    j++;
                    (function (number) {
                        currentJudge.save(function (err) {
                            if (err) return handleError(err);
                            console.log(i);
                            judgesSaveDone();
                        });
                    }(j));
                }

            }


            ], function (err) {
                if (err){
                    console.log(err);
                }
                console.log("baza danych dodana");
                    var lel = new dbControl({isCreated:true});
                    lel.save(function (err) {
                        if (err) return handleError(err);
                        createDone();
                    });
            }

        );






    }
    else{
        console.log("Baza danych juz istnieje");
        createDone();
    }


};

async.series([connectDB, initiateDB, createDB], function(err) {
    if (err) {
        console.log(err);
    }
    console.log("Process exit");
    process.exit(0);
}
);

var handleError = function(err)
{
    console.log(err);
};


var prefix = ["Fast", "Amazing", "Friendly", "Swift", "Majestic", "Adorable", "Glorious", "Fiery",
    "Noble", "Mighty", "Devious", "Intelligent", "Powerful", "Stubborn", "Smart", "Dirty", "Lazy",
"Sweet", "Clumsy"];

var horseNames = ["John", "Jack", "Ace", "Aiko", "Alexa", "Al", "Harry", "Herman", "Hailey", "Henry",
"Bambi", "Bart", "Basha", "Bastian", "Batman", "Cadance", "Candy", "Cash", "Cascade", "Cisco", "CJ",
"Isaac", "Ivan", "Igor", "Roach", "Rauli", "Ranger", "Randy", "Rainstorm", "Razz", "Rage", "Daisy",
"Damien", "Darkness", "Dawn", "Dax", "Daze", "Dazzle", "Lina", "Dante", "Dawson", " Dazzler", "Dee",
"Davis", "Danny"," Decker", "Danxor", "Eclipse", "Edgy", "Edgar", "Elzar", "Emerald","Encore","Enigma",
"EZ", "Rekt", "Scrub", "Noob", "Plox", "Hax", "Nadia","Nevarra","Nick", "No fear"];



var randomHorses = function (howMany) {
    var horses = [];

    for (var j = 0; j< howMany; j++ ){
        var hName = "";
        if ((Math.random()*100)+1 > 30) {
            hName  += prefix[Math.floor(Math.random()* prefix.length)];
            hName +=" ";
        }
        hName  += horseNames[Math.floor(Math.random()* horseNames.length)];
        horses.push(hName);
    }

    return horses;
};

var firstnames = ["John", "Adam", "Bob", "Sven", "James", "Trudy", "Paul", "Sam",
"David", "William", "Liam", "Lucas", "Frans", "Hans", "Jacob","Jack","Daniel",
"Antonio","Justin", "Sofia", "Maria", "Julia", "Emily", "Alice", "Luara", "Mia",
"Amanda", "Victoria", "Isabella", "Kamila", "Brianna", "Gabrielle"];
var surnames = ["Johnson", "Smith", "Kovalsky", "Chepter", "Adamson", "Jameson",
"Walker", "Bradd", "Lam", "Li", "Lee", "Wilson", "Martin", "Brown", "Bens", "Roy",
"Jones", "Martinez", "Anderson", "Jackson", "Lopez", "White", "Thompson",
"Harris", "Clark", "Taylor"];
var countries = ["USA", "Germany", "Mexico", "England", "France", "Canada", "Brasil"];

var randomJudges = function (howMany){
    var judges = [];

    for (var j = 0; j< howMany; j++ ){
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 5; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        var judge = {
            firstname: firstnames[Math.floor(Math.random() * firstnames.length)],
            surname: surnames[Math.floor(Math.random() * surnames.length)],
            country: countries[Math.floor(Math.random() * countries.length)],
            code: text
        };

        judges.push(judge);
    }


    return judges;
};