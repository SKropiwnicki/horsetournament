var mongoose = require('mongoose');


var Rating = mongoose.model('Rating', new  mongoose.Schema( {
    _horse:{type: Number, ref: 'Horse'},
    T: Number,
    G: Number,
    K: Number,
    N: Number,
    R: Number,
    _judge:{type: Number, ref: 'Judge'}
}));

module.exports = Rating;