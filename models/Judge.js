var mongoose = require('mongoose'), Schema = mongoose.Schema;
var Rating        = require('./Rating');

var Judge = mongoose.model('Judge',new mongoose.Schema({
    _id: Number,
    firstname: String,
    surname: String,
    country: String,
    code: String,
    ratings: [{type: Schema.Types.ObjectId, ref: 'Rating'}]
}));

module.exports = Judge;