
/* jshint node: true */
var app = require("express")();
var httpServer = require("http").Server(app);
var io = require("socket.io")(httpServer);
var Horse = require('./models/Horse');
var Rating = require('./models/Rating');
var Judge = require('./models/Judge');
var _=require('underscore');
var mongoose = require('mongoose');
var async = require('async');
var waterfall = require('async-waterfall');

var static = require('serve-static');
//var less = require('less-middleware');
var path = require('path');
var port = process.env.PORT || 3000;

var horseNumber = 5;
var judgesNumber = 10;
var judging = 5;
var currentHorseID = 0;
var currentJudges = [];
var allJudges = [];
var loggedJudges = [];
var ratedHorses = [];
var actualRatings = 0;
var lastHorse = null;

app.use('/js/jquery.min.js', static(__dirname + '/bower_components/jquery/dist/jquery.min.js'));
app.use(static(path.join(__dirname, '/public')));


mongoose.connect('mongodb://localhost/HorseTournament');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function () {
        console.log("Połaczone");
    });


    io.sockets.on("connection", function (socket) {

        socket.on("addRating", function (data) {
            console.log(data);

            Rating.findOneAndUpdate({_horse: currentHorseID, _judge: data._judge},
                {$set: {T: data.T, G: data.G, K: data.K, N: data.N, R: data.R} },
                {upsert: true},
                function(err)
                {
                    if (err) console.log(err);
                    socket.emit("msg", "Your verdict was added, you can change it, before we get to other horses");
                    actualRatings++;
                    socket.emit("getRatingsNumber", actualRatings, judging);
                });

        });

        socket.on("disconnect", function ()
        {
            console.log("Disconnect on socket id: " + socket.id);
            loggedJudges = _.reject(loggedJudges, function(object){
                return object.idSocket === socket.id;
            });
           // console.log("Zalogowani sedziowie liczba: " + loggedJudges.length);
        });

        socket.on("error", function (err) {
            console.dir(err);
        });
        socket.on("warnJudges", function(){
            io.emit("msgCanRate", "If you haven't rated yet, please do it now!")
        });
        socket.on("askForUpdate", function(){
            console.log("I was asked for update");
            var data=[];
            for (var i = 0; i < ratedHorses.length; i++) {
                var dataObject = {
                    name: ratedHorses[i].name,
                    averageAll: ratedHorses[i].averageAll,
                    averageT: ratedHorses[i].averageT,
                    averageR: ratedHorses[i].averageR
                };
                console.log("Sprawdzam dataObject",dataObject.averageAll);
                data.push(dataObject);
            }
            socket.emit("getActualView", data);
        });

        socket.on("changeJudges", function () {
            var thisJudge = _.find(loggedJudges, function(object){ return object.idSocket == socket.id;})
            if ( thisJudge === undefined ) {
                console.log("Niezalogowany sędzia");
            }
            else {
                console.log(thisJudge);
                if (_.find(currentJudges, function(object){ return object._id == thisJudge.idJudge;}) === undefined ) {
                    //nieaktywny sędzia
                    var answer1 ={msg:"It's not your turn to rate horse. Please wait untill you will be chosen", isChosen:false};
                    socket.emit("checkCanRate", answer1);
                }
                else {
                    var answer2 ={msg:"You were chosen to rate next horse", isChosen:true};
                    socket.emit("checkCanRate", answer2);
                }
            }
        });

        socket.on("nextHorse", function () {
                console.log("Zaczynam finalizowanie konia o id" + currentHorseID);
                var data = [];
                var dataRatings=[];

                async.series([ function (getHorseDone){
                        getHorse (currentHorseID, function(){

                            getHorseDone();

                        });
                }, function (judgesDone){

                    ChangeJudges( function (){

                        if (currentHorseID < horseNumber - 1) io.emit("checkJudges");
                    });

                    judgesDone();
                }, function (makeDataDone) {
                    console.log("Tablica konii posortowanych teoretycznie dlugosc " + ratedHorses.length);

                        console.log (lastHorse.name + " has " + lastHorse.ratings.length + "ratings" );

                        for(var i = 0;  i < lastHorse.ratings.length; i++){
                            console.log(lastHorse.ratings[i]._judge.firstname);
                            dataRatings.push(lastHorse.ratings[i]);
                        }

                        var dataObject = {
                            name: lastHorse.name,
                            averageAll: lastHorse.averageAll,
                            averageT: lastHorse.averageT,
                            averageR: lastHorse.averageR
                        };
                        console.log("Sprawdzam dataObject",dataObject.averageAll);
                        data = dataObject;

                    makeDataDone();
                }],function(){
                    io.emit("addRow", data);
                    io.emit("lastRatings", dataRatings, lastHorse.name);
                    if (currentHorseID >= horseNumber - 1) {
                        console.log("KONIEC ZAWODÓW");
                        socket.emit("finishAdmin");
                        io.emit("finish");
                    } else {
                        currentHorseID++;
                        initializeHorse(currentHorseID, function()
                        {
                            socket.emit("getRatingsNumber", actualRatings, judging);
                        });
                        io.emit("resetValues");
                        io.emit("msgCanRate", "New Horse! Please rate him fair.");
                        io.emit("getHorseID", currentHorseID);
                    }
                });
        });

        socket.on("askHorseID", function () {
            console.log("Zapytanie o horseID");
            socket.emit("getHorseID", currentHorseID);
        });

        socket.on("askRatingsNumber", function () {
           console.log("Zapytanie o ratingy konia " + currentHorseID + " ktory ma " + actualRatings);
            socket.emit("getRatingsNumber", actualRatings, judging)
        });


        socket.on("identification", function (code) {
            console.log("Zapytanie identyfikacyjne " + code);
            var isOK = false;
            for (var i=0; i<judgesNumber; i++){
                if (code === allJudges[i].code) {
                    isOK=true;
                    if (_.find(loggedJudges, function(object){ return object.idJudge == allJudges[i]._id;}) === undefined ) {
                        console.log("Nie ma takiego syndziego w zalogowanych");
                        var loggedInput = {idJudge: allJudges[i]._id, idSocket: socket.id};
                        loggedJudges.push(loggedInput);
                        console.log("Otrzymane zapytanie identyfikacyjne zostało zaakceptowane dla " + allJudges[i]._id);

                        if (_.find(currentJudges, function(object){ return object._id == allJudges[i]._id;}) === undefined ) {
                            console.log("Number of current Judges: " +  currentJudges.length);
                            var answerNope =   {
                                bool: true,
                                _idJudge: allJudges[i]._id,
                                msg:"Valid Code, Please wait untill you will be chosen to rate",
                                judgeName:allJudges[i].firstname+" "+allJudges[i].surname,
                                isChosen: false
                            };
                            socket.emit("idAnswer", answerNope);
                        }
                        else {
                            var answer =   {
                                bool: true,
                                _idJudge: allJudges[i]._id,
                                msg:"Valid Code, it's your turn to rate horse",
                                judgeName:allJudges[i].firstname+" "+allJudges[i].surname,
                                isChosen: true
                            };
                            socket.emit("idAnswer", answer);
                        }
                    }
                    else {
                        console.log ("Ten sedzia jest juz zalogowany");
                        var answer2 = {bool: false, _idJudge: -1, msg: "This person is already logged in"};
                        socket.emit("idAnswer", answer2);
                    }
                }
            }
            if (!isOK) {
                console.log("Niepoprawny kod identyfikacji");
                var answer1 = {bool: false, _idJudge: -1, msg: "Invalid Code"};
                socket.emit("idAnswer", answer1);
            }

        });
        socket.on("showCodes", function () {
            console.log("Wyswietlam kody");
            for (var i=0; i<judging; i++){
                console.log(currentJudges[i].firstname +" "+currentJudges[i].surname+" id: " + currentJudges[i]._id +" Kod do wpisania: "+ currentJudges[i].code);
            }
            socket.emit("msg", "Kody zostaly wypisane na serwerze");
        });
    });

//server things

    app.get('/judge', function (req, res) {
        res.sendFile(__dirname + '/public/JudgeView.html');
    });

    app.get('/adminHax', function (req, res) {
        res.sendFile(__dirname + '/public/AdminPanel.html');
    });

    httpServer.listen(port, function () {
        console.log('Serwer HTTP działa na porcie ' + port);
    });


//Functions

    var initializeHorse = function (horseID, callback){
        actualRatings = 0;
        async.series([ function(actualRatingsdone){
            Rating.find({_horse: horseID}, function(err, result) {
                if (err)  console.log(err);
                actualRatings = result.length;
                console.log("Mamy "+ actualRatings + " ratingow dla konia o id" + horseID);
                actualRatingsdone();
            });
        }], function (err){
            if (err) console.log(err);
            callback();
            }
        )
    }; initializeHorse(currentHorseID, function (){});


    var getHorse = function(horseID, callback){


        waterfall([
            function (horseFindDone) {
                var horse = {averageAll: 0, averageT: 0 , averageR: 0};
                Horse.findOne({ '_id': horseID }, function (err, result) {
                    if (err) console.log(err);
                    horse = result;
                    horseFindDone(null, horse);
                })
            },
            function (horse, ratingDone){

                Rating
                    .find({ _horse: horseID })
                    .populate('_judge')
                    .exec(function (err, result) {
                        if (err) console.log(err);
                        horse.ratings = result;
                        ratingDone(null, horse);
                    });

            },
            function (horse, AverageDone){
                var sum = 0;
                var sumT = 0;
                var sumR = 0;
                for(var i= 0; i < horse.ratings.length; i++){
                    sum += horse.ratings[i].T + horse.ratings[i].G + horse.ratings[i].K + horse.ratings[i].N + horse.ratings[i].R;
                    sumT += horse.ratings[i].T;
                    sumR += horse.ratings[i].R;

                }
                horse.averageAll = sum / horse.ratings.length;
                horse.averageT = sumT / horse.ratings.length;
                horse.averageR = sumR / horse.ratings.length;


                AverageDone(null, horse);
            }
        ], function (err, horse) {
            if (err){
                console.log(err);
            }
            console.log("Przed returnem" + horse);
            console.log("Przed returnem srednia" + horse.averageAll);
            lastHorse = horse;
            ratedHorses.push(horse);
            console.log(ratedHorses.length);
            callback();
        });

    };

    var ChangeJudges = function (callback) {

        var judges = [];
        currentJudges = [];
        Judge
            .find({}, function (err, res) {
                if (err) console.log(err);
                //console.log(res);
                judges = res;
                allJudges = judges;
                judgesNumber = judges.length;

                if (judging < judgesNumber) {
                    currentJudges = _.sample(judges, judging);
                } else currentJudges = _.sample(judges, judgesNumber);
                //console.log(currentJudges);
                callback();
            });
       // console.log(currentJudges);
    };
    ChangeJudges(function (){});

